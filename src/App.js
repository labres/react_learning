import React from 'react';
import {View, StyleSheet} from 'react-native';
import Botao from './componentes/Botao'
// import MinMax from './componentes/MinMax'
// import Aleatorio from './componentes/Aleatorio'

// import Titulo from './componentes/Titulo'

// import ComponenteDefault, { Comp1, Comp2 } from './componentes/Multi'
// import First from './componentes/Primeiro';

export default () => (
    <View style={style.App}>
        <Botao />

        {/*
         <Titulo
            principal="cadastro produto"
            secundario="tela de cadastro">
        </Titulo>
        <Aleatorio
            min={10}
            max={60}>
        </Aleatorio>
        <MinMax min="3" max="20"/> 
         <ComponenteDefault />
        <Comp1/>
        <Comp2 />
        <First />  */}
    </View>
);

const style = StyleSheet.create({
    App:{
        flexGrow: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 20
    }
})
