import React from 'react'
import {Button} from 'react-native'

export default props => {
    
    const executar = () => {
        console.warn('Exec')
    }
    
    return (

        <>
            <Button
                title="Executar"
                onPress={executar}>
            </Button> 
        
            <Button
                title="Executar 2"
                onPress={function(){
                    console.warn("exec 2")
                }}>
            </Button> 
        </>
    )
}