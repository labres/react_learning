import React from 'react'
import {Text} from 'react-native'
import Estilo from './estilo'

export default ({min, max}) => {

    const result = Math.floor(Math.random() * (max - min + 1) + min);
    return (
        <Text style={Estilo.big}>
            O valor aleatório entre {min} e {max} é {result} 
        </Text>
    )
}