import React, {Fragment} from 'react'
import {Text} from 'react-native'
import Estilo from './estilo'

export default props => (
  
        <Fragment>
            <Text style={Estilo.big}>
                {props.principal}
            </Text>

            <Text style={Estilo.small}>
                {props.secundario}
            </Text>
        </Fragment>
    
)
