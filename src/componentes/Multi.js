import React from 'react'
import { Text } from 'react-native'
import  Estilo from './estilo'

export default function comp() {
    return <Text style={Estilo.middle}>Comp #0ficial</Text>
}

function Comp1() {
    return <Text style={Estilo.middle}>Comp #02</Text>
}

function Comp2() {
    return <Text style={Estilo.middle}>Comp #03</Text>
}

export {Comp1, Comp2}
