import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    big:{
        fontSize: 32,
        textAlign: "center"
    },
    middle:{
        fontSize: 24,
        textAlign: "center"
    },
    small:{
        fontSize: 15,
        textAlign: "center"
    }
})